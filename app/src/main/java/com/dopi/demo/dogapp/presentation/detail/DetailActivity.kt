package com.dopi.demo.dogapp.presentation.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.dopi.demo.dogapp.R
import com.dopi.demo.dogapp.data.entities.Dog
import com.dopi.demo.dogapp.databinding.ActivityDetailBinding
import com.dopi.demo.dogapp.extensions.applyMaterialTransform
import com.dopi.demo.dogapp.presentation.base.DatabindingActivity

const val EXTRA_PARCELABLE_DOG = "EXTRA_PARCELABLE_DOG"

class DetailActivity : DatabindingActivity() {

    private val binding: ActivityDetailBinding by binding(R.layout.activity_detail)

    private val detailDog: Dog by lazy {
        intent.getParcelableExtra(EXTRA_PARCELABLE_DOG) as Dog
    }

    companion object {
        fun getStartIntent(
            context: Context,
            dog: Dog
        ): Intent {
            return Intent(context, DetailActivity::class.java)
                .putExtra(EXTRA_PARCELABLE_DOG, dog)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        applyMaterialTransform(detailDog.id)
        binding.apply {
            dog = detailDog
            activity = this@DetailActivity
            container = detailContainer
            fab = fabMore
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        onBackPressed()
        return true
    }
}