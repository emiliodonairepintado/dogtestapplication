package com.dopi.demo.dogapp.presentation.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dopi.demo.dogapp.R
import com.dopi.demo.dogapp.data.entities.Dog
import com.dopi.demo.dogapp.extensions.loadImage
import kotlinx.android.synthetic.main.item_dog.view.*
import kotlin.properties.Delegates

class DogAdapter(private val onItemClicked: (dog: Dog) -> Unit) :
    RecyclerView.Adapter<DogAdapter.DogViewHolder>() {

    lateinit var clickedHolder: DogViewHolder

    private var dogList: List<Dog> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DogViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_dog, parent, false)
        val holder = DogViewHolder(view)
        holder.itemView.setOnClickListener {
            if (holder.adapterPosition != RecyclerView.NO_POSITION) {
                clickedHolder = holder
                onItemClicked.invoke(dogList[holder.adapterPosition])
            }
        }
        return holder
    }

    override fun getItemCount(): Int = dogList.size

    override fun onBindViewHolder(holder: DogViewHolder, position: Int) {
        if (position != RecyclerView.NO_POSITION) {
            val dog: Dog = dogList[position]
            holder.bind(dog)
        }
    }

    fun updateData(newList: List<Dog>) {
        dogList = if (dogList.isEmpty()) {
            newList
        } else {
            dogList + newList
        }
    }

    class DogViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(dog: Dog) {
            itemView.itemImageView.loadImage(dog.url)
            itemView.item_poster_title.text = dog.breeds?.first()?.name
        }
    }
}