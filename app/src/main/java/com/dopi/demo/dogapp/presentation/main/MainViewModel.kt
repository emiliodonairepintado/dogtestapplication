package com.dopi.demo.dogapp.presentation.main

import androidx.lifecycle.MutableLiveData
import com.dopi.demo.dogapp.data.entities.Dog
import com.dopi.demo.dogapp.data.repositories.DogRepository
import com.dopi.demo.dogapp.presentation.base.BaseViewModel
import com.dopi.demo.dogapp.utils.SingleLiveEvent
import com.dopi.demo.dogapp.utils.UseCaseResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel(private val dogRepository: DogRepository) : BaseViewModel() {

    val showLoading = MutableLiveData<Boolean>()
    val dogList = MutableLiveData<List<Dog>>()
    val showError = SingleLiveEvent<String>()
    val navigateToDetail = SingleLiveEvent<Dog>()

    init {
        loadDogs()
    }

    internal fun loadDogs() {
        showLoading.value = true
        launch {
            val result = withContext(Dispatchers.IO) { dogRepository.getDogsList() }
            showLoading.value = false
            when (result) {
                is UseCaseResult.Success -> dogList.value = result.data.filter { !it.breeds.isNullOrEmpty() }
                is UseCaseResult.Error -> showError.value = result.exception.message
            }
        }
    }

    fun itemClicked(dog: Dog) {
        navigateToDetail.value = dog
    }
}
