package com.dopi.demo.dogapp.binding

import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dopi.demo.dogapp.data.entities.Dog
import com.dopi.demo.dogapp.extensions.loadImage
import com.dopi.demo.dogapp.presentation.main.adapter.DogAdapter

@BindingAdapter("loadImage")
fun bindLoadImage(view: AppCompatImageView, url: String) {
  view.loadImage(url)
}

@BindingAdapter("adapter")
fun bindAdapter(view: RecyclerView, baseAdapter: DogAdapter) {
  view.adapter = baseAdapter
}

@BindingAdapter("loadTitle")
fun loadTitle(view: AppCompatTextView, dog: Dog) {
    view.text = dog.breeds?.first()?.name
}

@BindingAdapter("loadGroup")
fun loadGroup(view: AppCompatTextView, dog: Dog) {
    view.text = dog.breeds?.first()?.breed_group
}

@BindingAdapter("loadTemperament")
fun loadTemperament(view: AppCompatTextView, dog: Dog) {
    view.text = dog.breeds?.first()?.temperament
}

