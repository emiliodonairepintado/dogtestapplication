package com.dopi.demo.dogapp.presentation.main

import android.app.ActivityOptions
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.dopi.demo.dogapp.R
import com.dopi.demo.dogapp.data.entities.Dog
import com.dopi.demo.dogapp.databinding.ActivityMainBinding
import com.dopi.demo.dogapp.extensions.applyExitMaterialTransform
import com.dopi.demo.dogapp.presentation.base.DatabindingActivity
import com.dopi.demo.dogapp.presentation.detail.DetailActivity
import com.dopi.demo.dogapp.presentation.main.adapter.DogAdapter
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : DatabindingActivity() {

    private val binding: ActivityMainBinding by binding(R.layout.activity_main)
    private val mainViewModel: MainViewModel by viewModel()
    private val onItemClicked: (dog: Dog) -> Unit = { mainViewModel.itemClicked(it) }

    override fun onCreate(savedInstanceState: Bundle?) {
        applyExitMaterialTransform()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViewModel()
        initListeners()
        binding.apply {
            viewModel = mainViewModel
            lifecycleOwner = this@MainActivity
            adapter = DogAdapter(onItemClicked)
        }
    }

    private fun initViewModel() {
        mainViewModel.dogList.observe(this, Observer { dogList ->
            binding.adapter?.let {
                it.updateData(dogList)
                itemSwipeToRefresh.isRefreshing = false
            }
        })

        mainViewModel.showLoading.observe(this, Observer { showLoading ->
            binding.lottieLoading.apply {
                visibility = if (showLoading) {
                    playAnimation()
                    View.VISIBLE
                } else {
                    cancelAnimation()
                    View.GONE
                }
            }
        })

        mainViewModel.showError.observe(this, Observer { showError ->
            Toast.makeText(this, showError, Toast.LENGTH_SHORT).show()
        })

        mainViewModel.navigateToDetail.observe(this, Observer { dog ->
            val intent = DetailActivity.getStartIntent(this, dog)
            val options = ActivityOptions.makeSceneTransitionAnimation(
                this,
                binding.adapter?.clickedHolder?.itemView,
                dog.id)
            startActivity(intent, options.toBundle())
        })
    }

    private fun initListeners() {
        binding.itemSwipeToRefresh.setOnRefreshListener {
            mainViewModel.loadDogs()
        }

        binding.dogsRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1)) {
                    mainViewModel.loadDogs()
                }
            }
        })
    }
}
