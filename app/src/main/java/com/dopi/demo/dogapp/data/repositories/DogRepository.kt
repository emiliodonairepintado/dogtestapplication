package com.dopi.demo.dogapp.data.repositories

import android.util.Log
import com.dopi.demo.dogapp.data.entities.Dog
import com.dopi.demo.dogapp.data.remote.DogApi
import com.dopi.demo.dogapp.utils.UseCaseResult

const val LIMIT = 100
const val IMAGE_SIZE = "med"
const val ORDER = "RANDOM"
const val PAGES = 10

interface DogRepository {
    suspend fun getDogsList(): UseCaseResult<List<Dog>>
}

class DogRepositoryImpl(private val dogApi: DogApi) :
    DogRepository {
    override suspend fun getDogsList(): UseCaseResult<List<Dog>> {
        return runCatching {
            val result = dogApi.getDogsAsync(
                limit = LIMIT,
                page = PAGES
            ).await()
            Log.d(this@DogRepositoryImpl.toString(), result.toString())
            UseCaseResult.Success(result)
        }.getOrElse {
            UseCaseResult.Error(it)
        }
    }
}
