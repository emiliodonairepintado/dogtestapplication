package com.dopi.demo.dogapp.extensions

import android.widget.ImageView
import coil.api.load

fun ImageView.loadImage(imageUrl: String) {
    this.load(imageUrl) {
        crossfade(true)
        scaleType = ImageView.ScaleType.CENTER_CROP
    }
}