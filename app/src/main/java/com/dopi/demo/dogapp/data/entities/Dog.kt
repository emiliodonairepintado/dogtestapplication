package com.dopi.demo.dogapp.data.entities

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Dog(
    @SerializedName("breeds")
    val breeds: List<Breed>? = emptyList(),
    @SerializedName("height")
    val height: Int,
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    val url: String,
    @SerializedName("width")
    val width: Int?
) : Parcelable

@Parcelize
data class Breed(
    @SerializedName("bred_for")
    val bred_for: String?,
    @SerializedName("breed_group")
    val breed_group: String?,
    @SerializedName("country_code")
    val country_code: String?,
    @SerializedName("height")
    val height: Height?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("life_span")
    val life_span: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("origin")
    val origin: String?,
    @SerializedName("temperament")
    val temperament: String?,
    @SerializedName("weight")
    val weight: Weight?
) : Parcelable

@Parcelize
data class Height(
    @SerializedName("imperial")
    val imperial: String?,
    @SerializedName("metric")
    val metric: String?
) : Parcelable

@Parcelize
data class Weight(
    @SerializedName("imperial")
    val imperial: String?,
    @SerializedName("metric")
    val metric: String?
) : Parcelable