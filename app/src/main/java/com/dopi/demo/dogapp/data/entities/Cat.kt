package com.dopi.demo.dogapp.data.entities

import com.google.gson.annotations.SerializedName

data class Cat(
    @SerializedName("id")
    val id: String,
    @SerializedName("url")
    val imageUrl: String,
    @SerializedName("sub_id")
    val sub_id: String,
    @SerializedName("categories")
    val categories: List<Categories> = emptyList()
)

data class Categories(
    @SerializedName("id")
    val id: String,
    @SerializedName("name")
    val name: String
)
