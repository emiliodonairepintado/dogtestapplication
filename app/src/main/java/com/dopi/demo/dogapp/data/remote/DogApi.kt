package com.dopi.demo.dogapp.data.remote

import com.dopi.demo.dogapp.data.entities.Cat
import com.dopi.demo.dogapp.data.entities.Dog
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface DogApi {
    @GET("images/search")
    fun getDogsAsync(
        @Query("limit") limit: Int,
        @Query("page") page: Int
    ): Deferred<List<Dog>>
}